package kreis;

//import java.awt.geom.Point2D;

public class Rechteck {
	//private Point2D punkt;
	private double sideA;
	private double sideB;
	private double circumference;
	private double area;
	
//	public double getPoint(){
//		return this.sideA;
//	}
//	public void setPoint(double neuX, double neuY) {
//		this.punkt.setLocation(neuX, neuY);
//	}
	
	public double getSideA(){
		return this.sideA;
	}
	public void setSideA(double neu) {
		this.sideA = neu;
	}
	
	public double getSideB(){
		return this.sideB;
	}
	public void setSideB(double neu) {
		this.sideB = neu;
	}
	
	public double getUmfang() {
		circumference = this.sideA*2 + this.sideB*2;
		return this.circumference;
	}
	
	public double getArea() {
		area = this.sideA*this.sideB;
		return area;
	}
	
	public Rechteck (double seiteA,double seiteB) {
		this.sideA = seiteA;
		this.sideB = seiteB;
//		this.punkt.setLocation(koordinateX, koordinateY);
	}
	public Rechteck() {	}
}
