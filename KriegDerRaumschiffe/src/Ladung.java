/**
 * @author Bergemann
 * @version V1
 */
public class Ladung {
	private int anzahl;
	private String name;
	/**
	 * 
	 * @return Die Anzahl des geladenen Objekts
	 */
	public int getAnzahl() {
		return anzahl;
	}
	/**
	 * 
	 * @param anzahl Eine neue Anzahl vom Objekt setzen
	 */
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	/**
	 * 
	 * @param anzahl Eine Anzahl zur schon existierenden Anzahl von einem Objekt hinzuf�gen
	 */
	public void addAnzahl(int anzahl) {
		this.anzahl = this.anzahl + anzahl;
	}
	/**
	 * 
	 * @return den Namen ausgeben vom Objekt
	 */
	public String getName() {
		return name;
	}
	/**
	 * 
	 * @param Namen neu setzen eines Objekts
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Leerer Konstruktor f�r ein Ladungs Objekt
	 */
	public Ladung () {
		this.name = "unknown";
		this.anzahl = 0;
	}
	/**
	 * Konstruktor
	 * @param name Name f�r ein Ladungsobjekt angeben
	 * @param anzahl Anzahl f�r ein Ladungsobjekt angeben
	 */
	public Ladung (String name, int anzahl) {
		this.name = name;
		this.anzahl = anzahl;
	}
	
//	+getAnzahl()
//	+setAnzahl(neuerWert : int)
//	+getName()
//	+setName(neuerName : String)
//	+Ladung()
//	+Ladung(name : String, anzahl : int)
}
