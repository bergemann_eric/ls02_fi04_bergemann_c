public class KriegDerRaumSchiffeTEST {
	
	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta",100,100,100,100,1,2);
		Raumschiff romulaner = new Raumschiff("IRW Khazara",100,100,100,100,2,2);
		Raumschiff vulkanier = new Raumschiff("NiVar",100,100,100,100,0,5);
		
		klingonen.addLadung(new Ladung("Ferengi Schneckensaft",200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert",200));
		romulaner.addLadung(new Ladung("Plasma-Waffe",50));
		romulaner.addLadung(new Ladung("Borg-Schrott",5));
		romulaner.addLadung(new Ladung("Rote Materie",2));
		vulkanier.addLadung(new Ladung("Forschungssonde",35));
		vulkanier.addLadung(new Ladung("Photonentorpedos",3));
		
			klingonen.photonentorpedosAbschiessen(romulaner);
			
			romulaner.phaserkanoneAbschiessen(klingonen);
			
			vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
			
			klingonen.zustandAusgeben();
			klingonen.ladungsverzeichnisAusgeben();
			
			vulkanier.reparaturAndroidenEinsetzen("alle", 5);
			vulkanier.photonentorpedosEinsetzen();
			vulkanier.ladungsverzeichnisAufraeumen();
			
			klingonen.photonentorpedosAbschiessen(romulaner);
			klingonen.photonentorpedosAbschiessen(romulaner);
			
			klingonen.zustandAusgeben();
			klingonen.ladungsverzeichnisAusgeben();
			
			romulaner.zustandAusgeben();
			romulaner.ladungsverzeichnisAusgeben();
			
			vulkanier.zustandAusgeben();
			vulkanier.ladungsverzeichnisAusgeben();
		
	}
	
}
