import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {
	private Random random = new Random();
	private String schiffName;
	private int energieversorgung;
	private int schutzschilde;
	private int lebenserhaltungssysteme;
	private int huelle;
	private int photonentorpedos;
	private int reparaturAndroiden;
	private static ArrayList<String> communicator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsLogbuch = new ArrayList<Ladung>();
	/**
	 * Leerer Konstruktor f�r ein Raumschiff
	 */
	public Raumschiff() {}
	/**
	 * Konstruktor f�r ein Raumschiff
	 * @param name Name eines Raumschiff Objekts
	 * @param energie Prozentuale Angabe von der Energieversorgung eines Raumschiff Objekts
	 * @param schutz Prozentuale Angabe von den Schutzschild eines Raumschiff Objekts
	 * @param leben Prozentuale Angabe von den Lebenserhaltungssystemen eines Raumschiff Objekts
	 * @param huelle Prozentuale Angabe von der H�lle eines Raumschiff Objekts
	 * @param photonen Anzahl der Photonentorpedos eines Raumschiff Objekts 
	 * @param androiden Anzahl der Reparaturandroiden eines Raumschiff Objekts 
	 */
	public Raumschiff(String name, int energie, int schutz, int leben, int huelle, int photonen, int androiden) {
		this.schiffName = name;
		this.energieversorgung = energie;
		this.schutzschilde = schutz;
		this.lebenserhaltungssysteme = leben;
		this.huelle = huelle;
		this.photonentorpedos = photonen;
		this.reparaturAndroiden = androiden;
		
		this.ladungsLogbuch = new ArrayList<Ladung>();
	}
	
	/**
	 * @return Zustand aller Attribute eines Raumschiff Objekts
	 */
	public void zustandAusgeben() {
		System.out.println("Name:                    " + this.schiffName);
		System.out.println("Energieversorgung:       " + this.energieversorgung);
		System.out.println("Schutzschilde:           " + this.schutzschilde);
		System.out.println("Lebenserhaltungssysteme: " + this.lebenserhaltungssysteme);
		System.out.println("H�lle:                   " + this.huelle);
		System.out.println("Protonentorpedos:        " + this.photonentorpedos);
		System.out.println("Reparatur Androiden:     " + this.reparaturAndroiden + "\n");
	}
	/**
	 * 
	 * @return den Namen des Schiffs
	 */
	public String getSchiffName() {
		return schiffName;
	}
	/**
	 * 
	 * @param schiffName Neuen Namen f�r das Schiff
	 */
	public void setSchiffName(String schiffName) {
		this.schiffName = schiffName;
	}
	/**
	 * 
	 * @return den Wert der Energieversorgung
	 */
	public int getEnergieversorgung() {
		return energieversorgung;
	}
	/**
	 * 
	 * @param energieversorgung den Wert der Energieversorgung neu setzen
	 */
	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}
	/**
	 * 
	 * @return den Wert der Schutzschilde
	 */
	public int getSchutzschilde() {
		return schutzschilde;
	}
	/**
	 * 
	 * @param schutzschilde den Wert der Schutzschilde neu setzen
	 */
	public void setSchutzschilde(int schutzschilde) {
		this.schutzschilde = schutzschilde;
	}
	/**
	 * 
	 * @return den Wert der Lebenserhaltungssysteme
	 */
	public int getLebenserhaltungssysteme() {
		return lebenserhaltungssysteme;
	}
	/**
	 * 
	 * @param lebenserhaltungssysteme den Wert der Lebenserhaltungssysteme neu setzen
	 */
	public void setLebenserhaltungssysteme(int lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}
	/**
	 * 
	 * @return den Wert der H�lle
	 */
	public int getHuelle() {
		return huelle;
	}
	/**
	 * 
	 * @param huelle den Wert der H�lle neu setzen
	 */
	public void setHuelle(int huelle) {
		this.huelle = huelle;
	}
	/**
	 * 
	 * @return die Anzahl der Photonentorpedos
	 */
	public int getPhotonentorpedos() {
		return photonentorpedos;
	}
	/**
	 * 
	 * @param photonentorpedos die Anzahl der Photonentorpedos neu setzen
	 */
	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}
	/**
	 * 
	 * @return die Anzahl der Reparaturandroiden
	 */
	public int getReparaturAndroiden() {
		return reparaturAndroiden;
	}
	/**
	 * 
	 * @param reparaturAndroiden die Anzahl der Reparaturandroiden neu setzen
	 */
	public void setReparaturAndroiden(int reparaturAndroiden) {
		this.reparaturAndroiden = reparaturAndroiden;
	}
	/**
	 * 
	 * @return Communicator Arraylist ausgeben
	 */
	public ArrayList<String> getCommunicator() {
		return communicator;
	}
	/**
	 * 
	 * @param broadcastCommunicator Communicator Arraylist neu setzen
	 */
	public void setCommunicator(ArrayList<String> broadcastCommunicator) {
		communicator = broadcastCommunicator;
	}
	/**
	 * 
	 * @return das Ladungslogbuch
	 */
	public ArrayList<Ladung> getLadungsLogbuch() {
		return ladungsLogbuch;
	}
	
	/**
	 * 
	 * Das Ladungsverzeichnis ausgeben auf der Konsole
	 */
	public void ladungsverzeichnisAusgeben() {		
		if(this.ladungsLogbuch.size() == 0) {
			System.out.println("Das Ladungsverzeichnis ist leer!\n");
		}
		else {
			System.out.println("Ladungsverzeichnis von " + this.schiffName + ":");
			for(int i = 0; i < this.ladungsLogbuch.size(); i++) {
				System.out.printf("%-30s",this.ladungsLogbuch.get(i).getName() + ":");
				System.out.println(this.ladungsLogbuch.get(i).getAnzahl());
			}
			
			System.out.println("");			
		}
	}
	/**
	 * 
	 * @param load eine Ladung zum Ladungsverzeichnis hinzuf�gen
	 */
	public void addLadung(Ladung load) {	
		int zaehler = 0;
		
			for(int i = 0; i < this.ladungsLogbuch.size(); i++) {
				if(this.ladungsLogbuch.get(i).getName().equals(load.getName())) {
					this.ladungsLogbuch.get(i).addAnzahl(load.getAnzahl());
					break;
				}
				zaehler += 1;
			}
			
		if(zaehler == this.ladungsLogbuch.size()) {
			this.ladungsLogbuch.add(load);
		}
		
		System.out.println(load.getAnzahl() + " von " +load.getName() + " wurde dem Ladungsverzeichnis hinzugef�gt!\n");
	}
	
	
	
	/**
	 * 
	 * @param name Gegnerisches Raumschiff treffen und diesem Punkte abziehen
	 */
	private void treffer(Raumschiff name) {
		System.out.println(name.getSchiffName() + " wurde getroffen!\n");
		name.setSchutzschilde(name.getSchutzschilde()-50);
		if(name.getSchutzschilde() <= 0) {
			name.setHuelle(name.getHuelle()-50);
			name.setLebenserhaltungssysteme(name.getLebenserhaltungssysteme()-50);
			
			if(name.getHuelle() <= 0) {
				name.setLebenserhaltungssysteme(0);
				nachrichtAnAlle("Die Lebenserhaltungssysteme von " + getSchiffName() + " wurde zerst�rt!");
			}
		}
	}
	/**
	 * 
	 * @param namen Photonentorpedos auf gegnerisches Raumschiff abfeuern
	 */
	public void photonentorpedosAbschiessen(Raumschiff namen) {
		if(this.photonentorpedos > 0) {
			System.out.println("Photonentorpedos abgeschossen!\n");
			this.photonentorpedos = this.photonentorpedos - 1;
			treffer(namen);
		}
		else {
			System.out.println("-=*Click*=-\n");
		}
	}
	/**
	 * 
	 * @param namen Phaserkanone auf gegnerisches Raumschiff abfeuern
	 */
	public void phaserkanoneAbschiessen(Raumschiff namen) {
		if(this.energieversorgung >= 50) {
			System.out.println("Phaserkanone abgeschossen!\n");
			this.energieversorgung = this.energieversorgung - 50;
			treffer(namen);
		}
		else {
			System.out.println("-=*Click*=-\n");
		}
	}
	/**
	 * die komplette Communicator Arraylist wiedergeben auf der Konsole
	 */
	public void broadcastCommunicator() {
		for(int i = 0; i < communicator.size(); i++) {
			System.out.println("Logbucheintrag " + (i+1) + ": " + communicator.get(i) + "\n");
		}
		if(communicator.size() == 0) {
			System.out.println("Es gibt noch keine Logbucheintr�ge\n");
		}
	}
	/**
	 * 
	 * @param nachricht in der Konsole wiedergeben und in die Communicator Arraylist eintragen
	 */
	public void nachrichtAnAlle(String nachricht){
		
		System.out.println(nachricht);
		
		communicator.add(getSchiffName() + ": " + nachricht);
	}
	/**
	 * Photonentorpedos dem Ladungsverzeichnis entfernen und dem Raumschiff hinzuf�gen
	 */
	public void photonentorpedosEinsetzen() {
		int zaehler = 0;
		for(int i = 0; i < this.ladungsLogbuch.size(); i++) {
			if(this.getLadungsLogbuch().get(i).getAnzahl() > 0 && this.ladungsLogbuch.get(i).getName() == "Photonentorpedos") {
				System.out.println("Es werden " + this.getLadungsLogbuch().get(i).getAnzahl() + " Photonentorpedo/s eingesetzt!\n");
				
				this.photonentorpedos += this.getLadungsLogbuch().get(i).getAnzahl();
				this.ladungsLogbuch.get(i).setAnzahl(0);
			}
			zaehler += 1;
		}
		
		if(zaehler != this.ladungsLogbuch.size()) {
			System.out.println("Keine Torpedos an Bord gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		}
		
	}
	/**
	 * falls ein Eintrag im Ladungslogbuch die Anzahl 0 hat wird dieser gel�scht
	 */
	public void ladungsverzeichnisAufraeumen() {
		for(int i = 0; i < this.ladungsLogbuch.size(); i++) {
			if(this.getLadungsLogbuch().get(i).getAnzahl() < 1) {
			this.ladungsLogbuch.remove(i);
			i = i - 1;
			}
		}
	}
	/**
	 * 
	 * @param s An welchen Parametern sollen die Androiden eingesetzt werden zum "reparieren"?
	 * @param eingesetzteAndroiden wie viele der vorhandenen Androiden sollen eingesetzt werden?
	 */
	public void reparaturAndroidenEinsetzen(String s, int eingesetzteAndroiden) {
		int x = random.nextInt(100);
		int zaehler = 0;
		if(eingesetzteAndroiden > this.getReparaturAndroiden()) {
			eingesetzteAndroiden = this.getReparaturAndroiden();
		}
		if(s.toLowerCase().contains("energieversorgung")){
		zaehler += 1;
		}
		if(s.toLowerCase().contains("schutzschild")){
			zaehler += 1;
		}
		if(s.toLowerCase().contains("h�lle")){
			zaehler += 1;
		}
		if(s.toLowerCase().contains("leben")){
			zaehler += 1;
		}
		if(s.toLowerCase().contains("alle")){
			zaehler += 4;
		}
		float healing = (x*eingesetzteAndroiden)/zaehler;
		
		if(s.toLowerCase().contains("energieversorgung")){
			this.energieversorgung = this.energieversorgung + (int)healing;
			System.out.println("Es wurde die Energieversorgung um " + (int)healing + " Punkte repariert");
		}
		if(s.toLowerCase().contains("schutzschild")){
			this.schutzschilde = this.schutzschilde + (int)healing;
			System.out.println("Es wurden die Schutzschilde um " + (int)healing + " Punkte repariert");
		}
		if(s.toLowerCase().contains("h�lle")){
			this.huelle = this.huelle + (int)healing;
			System.out.println("Es wurde die H�lle um " + (int)healing + " Punkte repariert");
		}
		if(s.toLowerCase().contains("leben")){
			this.lebenserhaltungssysteme = this.lebenserhaltungssysteme + (int)healing;
			System.out.println("Es wurden die Lebenserhaltungssysteme um " + (int)healing + " Punkte repariert");
		}
		if(s.toLowerCase().contains("alle")){
			this.energieversorgung = this.energieversorgung + (int)healing;
			this.schutzschilde = this.schutzschilde + (int)healing;
			this.huelle = this.huelle + (int)healing;
			this.lebenserhaltungssysteme = this.lebenserhaltungssysteme + (int)healing;
			System.out.println("Es wurden alle Systeme um " + (int)healing + " Punkte repariert");
		}
		System.out.println("");
	}
}
